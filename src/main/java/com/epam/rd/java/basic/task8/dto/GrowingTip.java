package com.epam.rd.java.basic.task8.dto;

import javax.print.attribute.standard.MediaSize;

public class GrowingTip {

    private Measure temperature;

    private String lightRequiring;

    private Measure watering;

    public GrowingTip(Measure temperature, String lightRequiring, Measure watering) {
        this.temperature = temperature;
        this.lightRequiring = lightRequiring;
        this.watering = watering;
    }

    public Measure getTemperature() {
        return temperature;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public Measure getWatering() {
        return watering;
    }

    @Override
    public String toString() {
        return "GrowingTip{" +
                "temperature=" + temperature +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", watering=" + watering +
                '}';
    }
}
