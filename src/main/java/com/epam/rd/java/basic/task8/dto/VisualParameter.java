package com.epam.rd.java.basic.task8.dto;

public class VisualParameter {

    private String stemColour;

    private String leafColour;

    private Measure aveLenFlower;

    public VisualParameter(String stemColour, String leafColour, Measure aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public Measure getAveLenFlower() {
        return aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameter{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }
}