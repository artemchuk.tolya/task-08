package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.dto.Flower;
import com.epam.rd.java.basic.task8.dto.GrowingTip;
import com.epam.rd.java.basic.task8.dto.Measure;
import com.epam.rd.java.basic.task8.dto.VisualParameter;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	private List<Flower> flowers = new ArrayList<>();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parseFile() throws ParserConfigurationException, IOException, SAXException {
		File inputFile = new File(xmlFileName);

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document document = db.parse(inputFile);

		collectDataFromDocument(document);
	}

	private void collectDataFromDocument(Document document) {
		NodeList nodeList = document.getElementsByTagName("flower");

		for (int i = 0; i < nodeList.getLength(); i++) {
			flowers.add(mapToFlower(nodeList.item(i)));
		}
	}

	private Flower mapToFlower(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element flowerElement = (Element) node;
			String name = getItem(flowerElement, "name").getTextContent();
			String soil = getItem(flowerElement, "soil").getTextContent();
			String origin = getItem(flowerElement, "origin").getTextContent();

			Element visualParams = (Element) getItem(flowerElement, "visualParameters");

			String aveLengthUnit = getItem(visualParams, "aveLenFlower").getAttributes().getNamedItem("measure").getTextContent();
			String aveLengthValue = getItem(visualParams, "aveLenFlower").getTextContent();
			Measure averageLength = new Measure(aveLengthUnit, aveLengthValue);

			String stemColour = getItem(visualParams, "stemColour").getTextContent();
			String leafColour = getItem(visualParams, "leafColour").getTextContent();
			VisualParameter vp = new VisualParameter(stemColour, leafColour, averageLength);

			Element growingTips = (Element) getItem(flowerElement, "growingTips");

			String temperatureUnit = getItem(growingTips, "tempreture").getAttributes().getNamedItem("measure").getTextContent();
			String temperatureValue = getItem(growingTips, "tempreture").getTextContent();
			Measure temperature = new Measure(temperatureUnit, temperatureValue);

			String wateringUnit = getItem(growingTips, "watering").getAttributes().getNamedItem("measure").getTextContent();
			String wateringValue = getItem(growingTips, "watering").getTextContent();
			Measure watering = new Measure(wateringUnit, wateringValue);

			String lightRequiring = getItem(growingTips, "lighting").getAttributes().getNamedItem("lightRequiring").getTextContent();
			GrowingTip gt = new GrowingTip(temperature, lightRequiring, watering);

			String multiplying = getItem(flowerElement, "multiplying").getTextContent();

			return new Flower(name, soil, origin, vp, gt, multiplying);
		}
		return null;
	}

	private Node getItem(Element element, String tagName) {
		return element.getElementsByTagName(tagName).item(0);
	}

	public void sort() {
		flowers.sort(Comparator.naturalOrder());
	}

	public void saveToFile(String name) throws ParserConfigurationException, TransformerException {
		File outputFile = new File(name);

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document document = db.newDocument();

		Element rootFlower = document.createElement("flowers");
		rootFlower.setAttribute("xmlns", "http://www.nure.ua");
		rootFlower.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		rootFlower.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
		document.appendChild(rootFlower);
		for (Flower flower : flowers) {
			System.out.println(flower);
			Element flowerEl = document.createElement("flower");
			mapFlowerToXml(document, flowerEl, flower);
			rootFlower.appendChild(flowerEl);
		}

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource domSource = new DOMSource(document);
		StreamResult streamResult = new StreamResult(outputFile);

		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.transform(domSource, streamResult);
	}

	private void mapFlowerToXml(Document document, Element fl, Flower flower) {
		Element flowerName = document.createElement("name");
		flowerName.setTextContent(flower.getName());
		fl.appendChild(flowerName);

		Element flowerSoil = document.createElement("soil");
		flowerSoil.setTextContent(flower.getSoil());
		fl.appendChild(flowerSoil);

		Element flowerOrigin = document.createElement("origin");
		flowerOrigin.setTextContent(flower.getOrigin());
		fl.appendChild(flowerOrigin);

		VisualParameter visualParameter = flower.getVisualParameter();
		Element flowerVisualParams = document.createElement("visualParameters");
		Element vpStemColour = document.createElement("stemColour");
		vpStemColour.setTextContent(visualParameter.getStemColour());
		flowerVisualParams.appendChild(vpStemColour);

		Element vpLeafColour = document.createElement("leafColour");
		vpLeafColour.setTextContent(visualParameter.getLeafColour());
		flowerVisualParams.appendChild(vpLeafColour);

		Element vpAverageLength = document.createElement("aveLenFlower");
		vpAverageLength.setTextContent(visualParameter.getAveLenFlower().getValue());
		Attr aveLenUnit = document.createAttribute("measure");
		aveLenUnit.setValue(visualParameter.getAveLenFlower().getUnit());
		vpAverageLength.setAttributeNode(aveLenUnit);
		flowerVisualParams.appendChild(vpAverageLength);

		fl.appendChild(flowerVisualParams);

		GrowingTip growingTip = flower.getGrowingTip();
		Element flowerGrowingTips = document.createElement("growingTips");

		Element vpTemperature = document.createElement("tempreture");
		vpTemperature.setTextContent(growingTip.getTemperature().getValue());
		Attr temperatureUnit = document.createAttribute("measure");
		temperatureUnit.setValue(growingTip.getTemperature().getUnit());
		vpTemperature.setAttributeNode(temperatureUnit);
		flowerGrowingTips.appendChild(vpTemperature);

		Element vpLighting = document.createElement("lighting");
		Attr lightRequiring = document.createAttribute("lightRequiring");
		lightRequiring.setValue(growingTip.getLightRequiring());
		vpLighting.setAttributeNode(lightRequiring);
		flowerGrowingTips.appendChild(vpLighting);

		Element vpWatering = document.createElement("watering");
		vpWatering.setTextContent(growingTip.getWatering().getValue());
		Attr wateringUnit = document.createAttribute("measure");
		wateringUnit.setValue(growingTip.getWatering().getUnit());
		vpWatering.setAttributeNode(wateringUnit);
		flowerGrowingTips.appendChild(vpWatering);

		fl.appendChild(flowerGrowingTips);

		Element flowerMultiplying = document.createElement("multiplying");
		flowerMultiplying.setTextContent(flower.getMultiplying());
		fl.appendChild(flowerMultiplying);
	}

}
