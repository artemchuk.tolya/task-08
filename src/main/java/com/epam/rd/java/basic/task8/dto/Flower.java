package com.epam.rd.java.basic.task8.dto;

import java.util.Comparator;

public class Flower implements Comparable<Flower> {

        private String name;

        private String soil;

        private String origin;

        private VisualParameter visualParameter;

        private GrowingTip growingTip;

        private String multiplying;

        public Flower(String name, String soil, String origin, VisualParameter visualParameter, GrowingTip growingTip, String multiplying) {
                this.name = name;
                this.soil = soil;
                this.origin = origin;
                this.visualParameter = visualParameter;
                this.growingTip = growingTip;
                this.multiplying = multiplying;
        }

        public String getName() {
                return name;
        }

        public String getSoil() {
                return soil;
        }

        public String getOrigin() {
                return origin;
        }

        public VisualParameter getVisualParameter() {
                return visualParameter;
        }

        public GrowingTip getGrowingTip() {
                return growingTip;
        }

        public String getMultiplying() {
                return multiplying;
        }

        @Override
        public String toString() {
                return "Flower{" +
                        "name='" + name + '\'' +
                        ", soil='" + soil + '\'' +
                        ", origin='" + origin + '\'' +
                        ", visualParameter=" + visualParameter +
                        ", growingTip=" + growingTip +
                        ", multiplying='" + multiplying + '\'' +
                        '}';
        }

        @Override
        public int compareTo(Flower o) {
                return name.compareTo(o.getName());
        }
}
