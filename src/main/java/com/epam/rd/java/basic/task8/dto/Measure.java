package com.epam.rd.java.basic.task8.dto;

public class Measure {

    private String unit;

    private String value;

    public Measure(String unit, String value) {
        this.unit = unit;
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Measure{" +
                "unit='" + unit + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
